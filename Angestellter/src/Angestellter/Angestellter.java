package Angestellter;

/**
 * Diese Klasse modelliert einen Angestellten 
 * @author Lorenz
 * @version 1.0 vom 26.03.2021
 *
 */

public class Angestellter {

	 private String name;
	 private String vorname;
	 private double gehalt;
	 //TODO: 3. Fuegen Sie in der Klasse 'Angestellter' das Attribut 'vorname' hinzu und implementieren Sie die entsprechenden get- und set-Methoden.
	 //TODO: 4. Implementieren Sie einen Konstruktor, der alle Attribute initialisiert.
	 //TODO: 5. Implementieren Sie einen Konstruktor, der den Namen und den Vornamen initialisiert.

	 public Angestellter() {
		 this.name = "Standardname";
		 this.vorname = "Standardvorname";
		 this.gehalt = 1500.00;
	 }
	 
	 /**
	  *  Vollparametrisierter Konstruktor fuer die Klasse Angestellter
	  * @param name Der Nachname des Angestellten
	  * @param vorname Der Vorname des Angestellten
	  * @param gehalt Das Gehalt des Angestellten
	  */
	 public Angestellter(String name, String vorname, double gehalt) {
//		 this.name = name;
//		 this.vorname = vorname;
		 this(name, vorname);
		 setGehalt(gehalt);
	 }
	 
	 /**
	  * Konstruktor fuer die Klasse Angestellter
	  * @param name Der Nachname des Angestellten
	  * @param vorname Der Vorname des Angestellten
	  */
	 public Angestellter (String name, String vorname) {
		 setGehalt(1500.0);
		 this.name = name;
		 this.vorname = vorname;
		 
	 }
	 
	 /**
	  *  Setzt den Nachnamen des Angestellten
	  * @param name
	  */
	 public void setName(String name) {
	    this.name = name;
	 }
	 
	 /**
	  *  Liefert den Vornamen des Angestellten
	  * @return
	  */
	 public String getVorname() {
		return vorname;
	}
	 
	/**
	 *  Setzt den Vornamen des Angestellten
	 * @param vorname
	 */
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	
	/**
	 *  Liefert den Nachnamen des Angestellten
	 * @return
	 */
	public String getName() {
	    return this.name;
	 }

	/**
	 *  Setzt das Gehalt des Angestellten
	 *  wenn der �bergebene Wert > 0.0 ist.
	 *  Sonst gibt es eine Fehlermeldung und tut nichts.
	 * @param gehalt
	 */
	 public void setGehalt(double gehalt) {
	   //TODO: 1. Implementieren Sie die entsprechende set-Methoden. 
	   //Ber�cksichtigen Sie, dass das Gehalt nicht negativ sein darf.
		 if (gehalt > 0.0) {
			 this.gehalt=gehalt;
		 }
		 else {
			 System.out.println("Ung�ltigert Wert.");
		 }
	 }
 
	 /**
	  * Liefert das Gehalt des Angestellten
	  * @return
	  */
	 public double getGehalt() {
	   //TODO: 2. Implementieren Sie die entsprechende get-Methoden.
		 return this.gehalt;
	 }
	 
	 /**
	  * Diese Methode gibt den vollstaendigen Namen als String zur�ck
	  * @return gebildeter String mit Vor- und Nachname 
	  */
	 //TODO: 6. Implementieren Sie eine Methode 'vollname', die den vollen Namen (Vor- und Zuname) als string zur�ckgibt.
	 public String vollname() {
		 String vollname = this.vorname + " " +  this.name;
		 return vollname;
	 }
}
