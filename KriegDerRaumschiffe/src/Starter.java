/**
 * 
 * Dies ist die Main Klasse f�r das Projekt Krieg der Raumschiffe
 * @author Lorenz Hankel
 * @version 0.1 vom 10.04.2021
 *
 */
public class Starter {

	/**
	 * Dies ist die Main Funktion des Projektes Krieg der Raumschiffe
	 * Sie f�hrt die vorgegebenen Konstruktoren aus der Moodle Aufgaben Liste aus,
	 * setzt die entsprechenden Parameter und f�hrt den vorgegebenen Ablauf aus.
	 * Die Vorgaben lauten wie folgt: <br>
	 * Auszuf�hrende Methoden in der Main:<br>
	 * Die Klingonen schie�en mit dem Photonentorpedo einmal auf die Romulaner.<br>
	 * Die Romulaner schie�en mit der Phaserkanone zur�ck.<br>
	 * Die Vulkanier senden eine Nachricht an Alle �Gewalt ist nicht logisch�.<br>
	 * Die Klingonen rufen den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus<br>
	 * Die Vulkanier sind sehr sicherheitsbewusst und setzen alle Androiden zur Aufwertung ihres Schiffes ein (f�r Experten).<br>
	 * Die Vulkanier verladen Ihre Ladung �Photonentorpedos� in die Torpedor�hren Ihres Raumschiffes und r�umen das Ladungsverzeichnis auf (f�r Experten).<br>
	 * Die Klingonen schie�en mit zwei weiteren Photonentorpedo auf die Romulaner.<br>
	 * Die Klingonen, die Romulaner und die Vulkanier rufen jeweils den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus.<br>
	 * @param args Kommandozeilenparameter
	 */
	public static void main(String[] args) {
		//Mainmethode des Programms Krieg der Raumschiffe
		
		//Objekte anhand des gegebenen Objektdiagramms erstellen auf LS02-5-02
		
		//Zuerst die Klingonen
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
		Ladung ferengiSchneckensaft = new Ladung("Ferengi Schneckensaft", 200);
		Ladung klingonenSchwerter = new Ladung("Bat'leth Klingonen Schwer", 200);
		klingonen.getLadungsverzeichnis().add(ferengiSchneckensaft);
		klingonen.getLadungsverzeichnis().add(klingonenSchwerter);
		
		//Anschlie�end die Romulaner
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
		Ladung borgSchrott = new Ladung("Borg-Schrott", 5);
		Ladung roteMaterie = new Ladung("Rote Materia", 2);
		Ladung plasmaWaffe = new Ladung("Plasma-Waffe", 50);
		romulaner.getLadungsverzeichnis().add(borgSchrott);
		romulaner.getLadungsverzeichnis().add(roteMaterie);
		romulaner.getLadungsverzeichnis().add(plasmaWaffe);
		
		//Abschlie�end die Vulkanier
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni'Var");
		Ladung forschungssonde = new Ladung("Forschungssonde", 35);
		Ladung photonentorpedo = new Ladung("Photonentorpedo", 3);
		vulkanier.getLadungsverzeichnis().add(forschungssonde);
		vulkanier.getLadungsverzeichnis().add(photonentorpedo);

		//Eigentlicher Programmablauf nach Moodle Vorlage
		klingonen.photonentorpedoSchiessen(romulaner);
		romulaner.phaserkanoneSchiessen(klingonen);
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
		klingonen.zustandAusgeben();
		klingonen.ladungsverzeichnisAusgeben();
		vulkanier.reparaturDurchfuehren(true, true, true, vulkanier.getAndroidenAnzahl());
		vulkanier.photonentorpedosLaden(3);
		vulkanier.ladungsverzeichnisAufraeumen();
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.zustandAusgeben();
		klingonen.ladungsverzeichnisAusgeben();
		romulaner.zustandAusgeben();
		romulaner.ladungsverzeichnisAusgeben();
		vulkanier.zustandAusgeben();
		vulkanier.ladungsverzeichnisAusgeben();
	}
}