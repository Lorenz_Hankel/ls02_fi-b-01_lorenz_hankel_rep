import java.util.ArrayList;
import java.util.Random;

/**
 * 
 * Diese Klasse modelliert ein Raumschiff mit diversen Methoden und Attributen
 * @author Lorenz Hankel
 * @version 0.1 vom 10.04.2021
 *
 */
public class Raumschiff {
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private static ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladungsverzeichnis;

	/**
	 * Voll Parametrisierter Konstruktor f�r die Klasse Raumschiff.
	 * Es werden alle Attribute f�r das neue Raumschiff Objekt initialisiert, sowie neue ArrayList f�r
	 * ladungsverzeichnis und broadcastKommunikator erstellt.
	 * @param photonentorpedoAnzahl int Die Anzahl der aktuell geladenen Photonentorpedos.
	 * @param energieversorgungInProzent int Prozentuelle Angabe des Zustandes der Energieversorgung des Raumschiffes.
	 * @param schildeInProzent int Prozentuelle Angabe des Zustandes der Schilde des Raumschiffes.
	 * @param huelleInProzent int Prozentuelle Angabe des Zustandes der H�lle des Raumschiffes.
	 * @param lebenserhaltungssystemeInProzent int Prozentuelle Angabe des Zustandes der Lebenserhaltungssysteme des Raumschiffes.
	 * @param androidenAnzahl int Die Anzahl der aktuell verf�gbaren Reparatur Androiden auf dem Raumschiff.
	 * @param schiffsname String Der Name des Raumschiffes.
	 */
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
		this.ladungsverzeichnis = new ArrayList<Ladung>();
		this.broadcastKommunikator = new ArrayList<String>();
	}
	
	/**
	 * Leerer Konstruktor f�r die Klasse Raumschiff
	 * Es werden Standardwerte gesetzt um Fehler zu vermeiden.
	 * photonentorpedoAnzahl = 0, energieversorgungInProzent = 55, schildeInProzent = 55, huelleInProzent = 55, 
	 * lebenserhaltungssystemeInProzent = 55, androidenAnzahl = 1, schiffsname = "TestRaumschiff", 
	 * ladungsverzeichnis = new ArrayList Ladung (), broadcastKommunikator = new ArrayList String ()
	 * 
	 * */
	public Raumschiff() {
		this.photonentorpedoAnzahl = 0;
		this.energieversorgungInProzent = 55;
		this.schildeInProzent = 55;
		this.huelleInProzent = 55;
		this.lebenserhaltungssystemeInProzent = 55;
		this.androidenAnzahl = 1;
		this.schiffsname = "TestRaumschiff";
		this.ladungsverzeichnis = new ArrayList<Ladung>();
		this.broadcastKommunikator = new ArrayList<String>();
	}

	/**
	 * Methode um neue Objekte der Klasse Ladung der ArrayList ladungsverzeichnis hinzuzuf�gen.
	 * @param neueLadung Das Objekt welches der ArrayList ladungsverzeichnis des Raumschiffes hinzugef�gt werden soll.
	 */
	public void addLadung(Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}
	
	/**
	 * Gibt alle relevanten Attribute des Raumschiffes auf der Konsole aus.
	 */
	public void zustandAusgeben() {
		//Die �blichen Attribute ausgeben.
		System.out.println("\n---- Start Ausgabe Zustand ----"
				+ "\nZustand des Raumschiffes: " + this.schiffsname
				+ "\nAnzahl der verf�gbaren Photonentorpedos: "+this.getPhotonentorpedoAnzahl()
				+ "\nEnergieversorung: "+this.getEnergieversorgungInProzent()+"%"
				+ "\nSchilde: "+this.getSchildeInProzent()+"%"
				+ "\nH�lle: "+this.getHuelleInProzent()+"%"
				+ "\nLebenserhaltungssysteme: "+this.getLebenserhaltungssystemeInProzent()+"%"
				+ "\nAnzahl verf�gbarer Reparatur Androiden: "+this.getAndroidenAnzahl()
				+ "\n---- Ende Ausgabe Zustand ----\n");
	}
	
	/**
	 * Durchl�uft die ArrayList ladungsverzeichnis des Raumschiffes und gibt die Attribute der darin gespeicherten Objekte
	 * in der Konsole aus.
	 */
	public void ladungsverzeichnisAusgeben() {
		System.out.println("\n---- Start Ausgabe Ladungsverzeichnis ----");
		for(int i = 0; i < this.getLadungsverzeichnis().size(); i++) {
			System.out.println("#"+ i + " Name: "+ this.getLadungsverzeichnis().get(i).getBezeichnung()
					+" Menge: "+ this.getLadungsverzeichnis().get(i).getMenge());
		}
		System.out.println("---- Ende Ausgabe Ladungsverzeichnis ----\n");
	}
	
	/**
	 * Diese Methode pr�ft ob Photonentorpedos geladen sind, wenn ja dann wird die Methode treffer mit dem Paramter r (dem Zielraumschiff) �bergeben.
	 * Sollte die Bedingung nicht erf�llt sein, so wird die Methode nachrichtAnAlle mit dem Parameter "-=*Click*=-" ausgef�hrt.
	 * @param r Das Zielobjekt (Raumschiff), welches von dem Photonentorpedo getroffen werden soll.
	 * 
	 */
	public void photonentorpedoSchiessen(Raumschiff r) {
		//checken ob Photonentorpedos vorhanden sind
		if (this.getPhotonentorpedoAnzahl()<=0) {
			nachrichtAnAlle("-=*Click*=-");
		}
		else {
			this.setPhotonentorpedoAnzahl(this.getPhotonentorpedoAnzahl()-1);
			nachrichtAnAlle("Photonentorpedo abgeschossen");
			treffer(r);
		}
	}
	
	/**
	 * Die Methode phaserkanoneSchiessen pr�ft erst ob die energieversorungInProzent kleiner 50 ist.
	 * Wenn nein: dann wird die Methode nachrichtAnAlle mit dem Parameter "Phaserkanone abgeschossen" und
	 * die Methode treffer mit dem Parameter r Objekt (Raumschiff) ausgef�hrt.
	 * Wenn ja: dann wird die Methode nachrichtAnAlle mit dem Parameter "-=*Click*=-" ausgef�hrt.
	 * @param r Das Zielobjekt (Raumschiff) welches von der Phaserkanone getroffen werden soll.
	 *
	 */
	public void phaserkanoneSchiessen(Raumschiff r) {
		//checken ob mehr als 50% Energieversorgung vorhanden sind.
		if (this.getEnergieversorgungInProzent()<50) {
			nachrichtAnAlle("-=*Click*=-");
		}
		else {
			this.setEnergieversorgungInProzent(this.getEnergieversorgungInProzent()-50);
			nachrichtAnAlle("Phaserkanone abgeschossen");
			treffer(r);
		}
	}
	
	/**
	 * Diese Methode f�gt den �bergebenen Parameter nachricht der ArrayList broadcastKommunikator hinzu.
	 * Die Anf�nger-Version hat den String parameter nachricht auf die Konsole ausgegeben.
	 * @param nachricht String Ein String welcher der ArrayList broadcastKommunikator hinzugef�gt wird, und auf der Konsole ausgegeben wird.
	 */
	public void nachrichtAnAlle(String nachricht) {
		broadcastKommunikator.add(nachricht);
		System.out.println(nachricht); //Anf�nger Version
	}
	
	/**
	 * Die Methode treffer mit dem Parameter r (Ziel-Raumschiff-Objekt) f�hrt die Schadensberechnung f�r getroffene Raumschiff Objekte durch.
	 * Es wird zuerst die Konsolennachricht ausgegeben, welches Raumschiff getroffen wurde.
	 * Daraufhin werden die Schilde des Raumschiffs r um 50 reduziert.
	 * Sollte daraufhin daraufhin schildeInProzent kleiner gleich 0 sein, so werden huelleInProzent und energieversorgungInProzent jeweils 50 abgezogen.
	 * Ist huelleInProzent nun auf 0 gesunken, so werden lebenserhaltungssystemeInProzent auch auf 0 gesetzt und 
	 * es wird die Methode nachrichtAnAlle ausgef�hrt, mit dem Parameter dass die Lebenserhaltungssysteme vernichtet worden sind.
	 * @param r Raumschiff Objekt Ziel des Treffers.
	 */
	private void treffer(Raumschiff r) {
		System.out.println(r.getSchiffsname()+" wurde getroffen!");
		r.setSchildeInProzent(r.getSchildeInProzent()-50);
		if (r.getSchildeInProzent()<0) {
			r.setHuelleInProzent(r.getHuelleInProzent()-50);
			r.setEnergieversorgungInProzent(r.getEnergieversorgungInProzent()-50);
			if (r.getHuelleInProzent()<=0) {
				r.setLebenserhaltungssystemeInProzent(0);
				nachrichtAnAlle("Die Lebenserhaltungssysteme von dem Raumschiff "+ r.getSchiffsname()+" wurden zerst�rt!");
			}
		}
	}
	
	/**
	 * Diese Methode soll das Nachladen von Photonentorpedos aus der Ladung in die Photonentorpedorohre simulieren.
	 * Es wird die ArrayList ladungsverzeichnis des Objektes durchlaufen um nach Objekten zu suchen, welche eine Bezeichnung haben
	 * welche "photonentorpedo" enth�lt.
	 * Der entsprechende Index der ArrayListe werden in dem Attribut int index gespeichert.
	 * Anschlie�end wird mithilfe des nun bekannten index abgeglichen ob die angegebene Menge zu der angeforderten Menge an Torpedos passt.
	 * Sollte die tats�chliche Menge 0 sein, so wird die Konsolennachricht "Keine Photonentorpedos gefunden!" ausgegeben, und die 
	 * Methode nachrichtAnAlle mit dem Parameter "-=*Click*=-" ausgef�hrt.
	 * Sollte die Menge gr��er 0, jedoch Menge kleiner zuLadendeTorpedos sein, so werden stattdessen alle tats�chlich verf�gbaren Torpedos dem Attribut
	 * photonentorpedoAnzahl dem Raumschiff hinzugef�gt und die Menge in der ladungsliste auf 0 gesetzt.
	 * Sollte die Menge gr��er 0 und Menge gr��er zuLadendeTorpedos sein, so werden die Menge der angeforderten Torpedos dem Attribut photonentorpedoAnzahl
	 * dem Raumschiff hinzugef�gt und dieselbe Menge der Menge des Objektes Ladung abgezogen. 
	 * @param zuLadendeTorpedos int Die Anzahl der Photonentorpedos, welche in die Torpedorohre (photonentorpedoAnzahl) geladen werden sollen.
	 */
	public void photonentorpedosLaden(int zuLadendeTorpedos) {
		int index=-1;
		int anzahlVorhandeneTorpedos=-1;
		
		for(int i = 0; i < this.getLadungsverzeichnis().size(); i++) {
			if (this.getLadungsverzeichnis().get(i).getBezeichnung().trim().toLowerCase().contains("photonentorpedo")){
				index = i;
				anzahlVorhandeneTorpedos = this.getLadungsverzeichnis().get(i).getMenge();
			}
		}
		if(anzahlVorhandeneTorpedos>=zuLadendeTorpedos) {
			this.setPhotonentorpedoAnzahl(this.getPhotonentorpedoAnzahl()+zuLadendeTorpedos);
			this.getLadungsverzeichnis().get(index).setMenge(this.getLadungsverzeichnis().get(index).getMenge()-zuLadendeTorpedos);
		}
		else if(anzahlVorhandeneTorpedos<zuLadendeTorpedos&&anzahlVorhandeneTorpedos>0) {
			this.setPhotonentorpedoAnzahl(this.getPhotonentorpedoAnzahl()+anzahlVorhandeneTorpedos);
			this.getLadungsverzeichnis().get(index).setMenge(0);
		}
		else {
			System.out.println("Keine Photonentorpedos gefunden!");
			this.nachrichtAnAlle("-=*Click*=-");
		}
	}
	
	/**
	 * Diese Methode durchl�uft die ArrayList ladungsverzeichnis des Raumschiff Objektes und sucht nach Objekten mit dem Attribut Menge kleiner gleich 0.
	 * Wenn diese gefunden werden, werden die Objekte aus der ArrayList ladungsverzeichnis gel�scht.
	 * Zudem wird ein interner Z�hler anzahlGeloeschterEintraege hochgez�hlt.
	 * Nachdem die Schleife durchgelaufen ist, wird eine Konsolennachricht ausgegeben, mit der Anzahl der gel�schten Eintr�ge.
	 */
	public void ladungsverzeichnisAufraeumen() {
		int anzahlGeloeschterEintraege = 0;
		for(int i = 0; i < this.getLadungsverzeichnis().size(); i++) {
			if(this.getLadungsverzeichnis().get(i).getMenge()<=0) {
				this.getLadungsverzeichnis().remove(i);
				anzahlGeloeschterEintraege +=1;
			}
		}
		System.out.println("Es wurden "+anzahlGeloeschterEintraege+" Eintr�ge aus dem Ladungsverzeichnis gel�scht.");
	}

	/**
	 * Laut Anforderung soll diese Methode den Broadcastkommunikator des Raumschiffes zur�ckgeben.
	 * @return ArrayList String der broadcastKommunikator des Raumschiffes der zur�ckgegeben wird.
	 */
	public ArrayList<String> eintraegeLogbuchZur�ckgeben() {
		//Ehrlich gesagt, verwirrt mich die Anforderung auf Moodle.
//
//	    Logbuch Eintr�ge zur�ckgeben (f�r Fortgeschrittene und Experten, 5 Punkte)
//
//	        Gibt den broadcastKommunikator zur�ck.

		return this.getBroadcastKommunikator();
	}
	
/**
 * Diese Methode pr�ft welche Schiffstruktur (schildeInProzent, energieversorgungInProzent, huelleInProzent) repariert werden soll,
 * z�hlt die Anzahl der zu reparierenden Strukturen mithilfe des int anzahlDerZuReparierendenSchiffsstrukturen und berechnet dann mithilfe einer Formel
 * die effektive Reparatur.
 * Es wird eine Zufallszahl zwischen 1 und 100 int erstellt.
 * Mithilfe der Formel : (zufallsZahl*anzahlDerEingesetztenDroiden)/(anzahlDerZuReparierendenSchiffsstrukturen) wird diese in der 
 * Variabel int ergebnisBerechnung zwischengespeichert.
 * Daraufhin wird den zu reparierenden Strukturen der Wert aus ergebnisBerechnung den Settern �bergeben (Die Setter haben jetzt eine
 * Pr�f-Verzweigung, um Werte �ber 100 abzufangen und auf 100 zu setzen).
 * @param schutzschilde boolean Wenn True dann wird versucht schutzschilde zu reparieren.
 * @param energieversorgung boolean Wenn True dann wird versucht energieversorgung zu reparieren.
 * @param schiffshuelle boolean Wenn True dann wird versucht schiffshuelle zu reparieren.
 * @param anzahlDroiden int Die Anzahl der Droiden die genutzt werden soll.
 */
	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden) {
		Random zufall = new Random();
		int zufallsZahl = zufall.nextInt(100)+1;
		int anzahlDerZuReparierendenSchiffsstrukturen = 0;
		int anzahlDerEingesetztenDroiden = 0;
		//Androidenanzahl pr�fen und setzen f�r sp�tere Berechnung
		if (anzahlDroiden > this.getAndroidenAnzahl()) {
			anzahlDerEingesetztenDroiden = this.getAndroidenAnzahl();
		}
		else {
			anzahlDerEingesetztenDroiden = anzahlDroiden;
		}
		//Z�hlen der zu reparierenden Strukturen
		if (schutzschilde == true) {
			anzahlDerZuReparierendenSchiffsstrukturen +=1;
		}
		if (energieversorgung == true) {
			anzahlDerZuReparierendenSchiffsstrukturen +=1;
		}
		if (schiffshuelle == true) {
			anzahlDerZuReparierendenSchiffsstrukturen +=1;
		}
		//Berechnung der effektiven Reparatur anhand der Formel: (ZufallsZahl*AnzahlderAndroiden)/(AnzahlDerZuReparierendenStrukturen)
		int ergebnisBerechnung = 0;
		ergebnisBerechnung = (zufallsZahl*anzahlDerEingesetztenDroiden)/(anzahlDerZuReparierendenSchiffsstrukturen);
		//Setten der neuen Werte
		//DEBUG
		System.out.println("\n---- DEBUG ----- \n Ergebnis der Berechnung: "+ergebnisBerechnung
		+"\n---- DEBUG ----\n");
		if(schutzschilde==true) {
			this.setSchildeInProzent(this.getSchildeInProzent()+ergebnisBerechnung);
		}
		if(energieversorgung == true) {
			this.setEnergieversorgungInProzent(this.getEnergieversorgungInProzent()+ergebnisBerechnung);
		}
		if(schiffshuelle == true) {
			this.setHuelleInProzent(this.getHuelleInProzent()+ergebnisBerechnung);
		}
	}
	
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	/**
	 * Diese Methode setzt das Attribut energieversorgungInProzent f�r ein Raumschiff Objekt.
	 * Die Methode wurde angepasst um illegale Werte �ber 100 und unter 0 zu vermeiden.
	 * Zu hohe Werte werden auf 100 gesetzt, zu niedrige Werte auf 0.
	 * @param energieversorgungInProzent int Der �bergebene Parameter, welche das Attribut energieversorgungInProzent des Raumschiff Objektes
	 * setzen soll.
	 */
	//Methode wurde angepasst um ung�ltige Eingaben zu vermeiden.
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		if (energieversorgungInProzent>=0 && energieversorgungInProzent<=100) {
		this.energieversorgungInProzent = energieversorgungInProzent;
		}
		else if(energieversorgungInProzent>100) {
			this.energieversorgungInProzent = 100;
		}
		else {
			this.energieversorgungInProzent = 0;
		}
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}
	/**
	 * Diese Methode setzt das Attribut schildeInProzent f�r ein Raumschiff Objekt.
	 * Die Methode wurde angepasst um illegale Werte �ber 100 und unter 0 zu vermeiden.
	 * Zu hohe Werte werden auf 100 gesetzt, zu niedrige Werte auf 0.
	 * @param schildeInProzent int Der �bergebene Parameter, welche das Attribut schildeInProzent des Raumschiff Objektes
	 * setzen soll.
	 */
	//Methode wurde angepasst um ung�ltige Eingaben zu vermeiden.
	public void setSchildeInProzent(int schildeInProzent) {
		if (schildeInProzent>=0 && schildeInProzent<=100) {
		this.schildeInProzent = schildeInProzent;
		}
		else if(schildeInProzent>100) {
			this.schildeInProzent = 100;
		}
		else {
			this.schildeInProzent = 0;
		}
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}
	
	/**
	 * Diese Methode setzt das Attribut huelleInProzent f�r ein Raumschiff Objekt.
	 * Die Methode wurde angepasst um illegale Werte �ber 100 und unter 0 zu vermeiden.
	 * Zu hohe Werte werden auf 100 gesetzt, zu niedrige Werte auf 0.
	 * @param huelleInProzent int Der �bergebene Parameter, welche das Attribut huelleInProzent des Raumschiff Objektes
	 * setzen soll.
	 */
	//Methode wurde angepasst um ung�ltige Eingaben zu vermeiden.
	public void setHuelleInProzent(int huelleInProzent) {
		if (huelleInProzent>=0 && huelleInProzent<=100) {
		this.huelleInProzent = huelleInProzent;
		}
		else if(huelleInProzent>100) {
			this.huelleInProzent = 100;
		}
		else {
			this.huelleInProzent = 0;
		}
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public static ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public static void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		Raumschiff.broadcastKommunikator = broadcastKommunikator;
	}

	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	public void setLadungsverzeichnis(ArrayList<Ladung> Ladungsverzeichnis) {
		this.ladungsverzeichnis = Ladungsverzeichnis;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
		
}
