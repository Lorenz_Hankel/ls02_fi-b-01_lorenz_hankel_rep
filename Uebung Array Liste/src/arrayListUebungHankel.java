import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class arrayListUebungHankel {

	public static void main(String[] args) {
		//Erzeugen des Zufallsgenerators
		Random zufall = new Random();
		Scanner eingabe = new Scanner(System.in);
		
		ArrayList<Integer> myList = new ArrayList<>(); 
		System.out.println("Ausgabe der Liste mit 20 Zufallszahlen zwischen 1 und 9.");
		for(int i = 0; i < 20; i++) {
			myList.add(zufall.nextInt(8)+1);
			//Ausgabe der Listenelemente auf Konsole
			System.out.println("myList["+i+"] : "+myList.get(i));
		}
		
		//Gesuchte Zahl abfragen
		System.out.println("\nBitte eine Suchzahl eingeben!\n");
		int suchzahl = eingabe.nextInt();
		
		//Z�hler f�r die Menge der gesuchten Indices initialisieren und deklarieren
		int z�hlerSuchZahl = 0;
		//For Schleife zum Z�hlen der Eintr�ge
		for(int i = 0; i < myList.size(); i++) {
			if (myList.get(i)==suchzahl) {
				z�hlerSuchZahl ++;
			}
		
		}
		
		System.out.println("Die Suchzahl befindet sich " + z�hlerSuchZahl + " mal in der ArrayList.");
		
		
		schlafen(1500);
		
		System.out.println("Die Zahl kommt an folgenden Indices in der Liste vor: ");
		
		//Ausgabe der Indices mit der Suchzahl
		for(int i = 0; i < myList.size(); i++) {
			if (myList.get(i)==suchzahl) {
				System.out.println("myList["+i+"] : "+myList.get(i));
			}
		}
		
		
		//L�schen der entsprechenden Indices
		for(int i = 0; i < myList.size(); i++) {
			if(myList.get(i)==suchzahl) {
				myList.remove(i);	
			}
		}
		
		schlafen(1500);
		
		System.out.println("\nListe nach L�schung von 8:");
		//Ausgabe nach dem L�schen
		for(int i = 0; i < myList.size(); i++) {
			System.out.println("myList["+i+"] : "+myList.get(i));
		}
		
		//Einf�gen einer 0 hinter jeder 5 in der Liste
		System.out.println("\nEinf�gen einer 0 hinter jeder 5 in der Liste");
		for(int i = 0; i < myList.size(); i++) {
			if (myList.get(i)==5) {
				myList.add(i+1, 0);
			}
		}
		schlafen(1500);
		System.out.println("\nListe nach Einfuegen von 0 hinter jeder 5:");
		//Letzte Ausgabe der Liste
		for(int i = 0; i < myList.size(); i++) {
			System.out.println("myList["+i+"] : "+myList.get(i));
		}
		
		eingabe.close();
		System.exit(0);
		
	}
	
	public static void schlafen(int zeitinms) {
		try {
			Thread.sleep(zeitinms);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
